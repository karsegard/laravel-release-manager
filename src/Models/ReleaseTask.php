<?php

namespace KDA\Laravel\ReleaseManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ReleaseManager\ServiceProvider;

class ReleaseTask extends Model
{
    use HasFactory;

    protected $fillable=  [
        'name',
        'description'
    ];

    public function getTable()
    {
        return ServiceProvider::getTableName('release_tasks');
    }

    protected static function newFactory()
    {
        return LeNomdeLaFactory::new();
    }
    public function release(){
        return $this->belongsTo(Release::class);
    }
}
