<?php

namespace KDA\Laravel\ReleaseManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ReleaseManager\ServiceProvider;

class Release extends Model
{
    use HasFactory;
    protected $fillable=  [
        'name',
        'description'
    ];
    public function getTable()
    {
        return ServiceProvider::getTableName('releases');
    }

    protected static function newFactory()
    {
        return LeNomdeLaFactory::new();
    }
    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function deployments(){
        return $this->hasMany(Deployment::class);
    }
    public function tasks(){
        return $this->hasMany(ReleaseTask::class);
    }
}
