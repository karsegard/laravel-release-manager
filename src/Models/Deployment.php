<?php

namespace KDA\Laravel\ReleaseManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use KDA\Laravel\ReleaseManager\ServiceProvider;

class Deployment extends Model
{
    use HasFactory;
    protected $fillable=  [
        'date',
        'release_id',
        'environment_id'
    ];

    public function getTable()
    {
        return ServiceProvider::getTableName('deployments');
    }

    protected static function newFactory()
    {
        return LeNomdeLaFactory::new();
    }

    public function release(){
        return $this->belongsTo(Release::class);
    }

    public function tasks(){
        return $this->belongsToMany(ReleaseTask::class)->withPivot('id','complete');
    }

    public function environment(){
        return $this->belongsTo(Environment::class);
    }

    public function syncTasks(){
        $this->tasks()->sync($this->release->tasks->pluck('id'),['complete'=>false]);
    }
   
}
