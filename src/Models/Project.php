<?php

namespace KDA\Laravel\ReleaseManager\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use KDA\Laravel\PackageManager\Models\Package;
use KDA\Laravel\ReleaseManager\ServiceProvider;

class Project extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable=[
        'name'
    ];
    public function getTable()
    {
        return ServiceProvider::getTableName('projects');
    }

    protected static function newFactory()
    {
        return LeNomdeLaFactory::new();
    }
    public function main_package(){
        return $this->belongsTo(Package::class,'package_id');
    }
    public function packages(){
        return $this->belongsToMany(Package::class);
    }

    public function releases(){
        return $this->hasMany(Release::class);
    }


    public function environments(){
        return $this->hasMany(Environment::class);
    }
}
