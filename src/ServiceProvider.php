<?php

namespace KDA\Laravel\ReleaseManager;

use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\Traits\HasConfig;
use KDA\Laravel\Traits\HasConfigurableTableNames;
use KDA\Laravel\Traits\HasDumps;
use KDA\Laravel\Traits\HasLoadableMigration;
use KDA\Laravel\Traits\HasMigration;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;
    use HasDumps;
    use HasMigration;
    use HasLoadableMigration;
    use HasConfigurableTableNames;


    protected $dumps = [
        'projects',
        'package_project',
        'environments',
        'releases',
        'deployments',
        'deployment_release_task',
        'release_tasks'
    ];
    protected static $tables_config_key="kda.release-manager.tables";

    protected $packageName = 'laravel-release-manager';
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    // trait \KDA\Laravel\Traits\HasConfig; 
    //    registers config file as 
    //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';
    protected $configs = [
        'kda/release-manager.php'  => 'kda.release-manager'
    ];

    public function register()
    {
        parent::register();
    }
    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }
    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
