<?php
// config for KDA/Laravel\ReleaseManager
return [
    'tables'=>[
        'projects'=>'projects',
        'deployments'=>'deployments',
        'environments'=>'environments',
        'releases'=>'releases',
        'release_tasks'=>'release_tasks',
        'deployment_release_task'=>'deployment_release_task',
        'package_project'=>'package_project'
    ]
];
