<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use KDA\Laravel\ReleaseManager\ServiceProvider;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ServiceProvider::getTableName('deployments'), function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->foreignId('environment_id');
            $table->foreignId('release_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ServiceProvider::getTableName('deployments'));
    }
};
